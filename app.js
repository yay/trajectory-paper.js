function getClosestPointIndex(position, step) {
    return [
        Math.round(position.x / step),
        Math.round(position.y / step)
    ];
}

function getClosestPoint(position, step) {
    return new Point(
        Math.round(position.x / step) * step,
        Math.round(position.y / step) * step
    );
}

var step = 20;
var viewCenter = getClosestPoint(view.center, step);
var gridPoints;

function createGrid(size, step) {
    var colCount = Math.round(size.width / step),
        rowCount = Math.round(size.height / step),
        points = [],
        colIdx, rowIdx, path;

    for (colIdx = 0; colIdx < colCount; colIdx++) {
        for (rowIdx = 0; rowIdx < rowCount; rowIdx++) {
            path = new Path.Circle({
                center: [colIdx * step, rowIdx * step],
                radius: 1,
                fillColor: 'black'
            });
            points[rowIdx * colCount + colIdx] = path;
        }
    }

    points.get = function (column, row) {
        if (arguments.length === 1) {
            row = column[1];
            column = column[0];
        }
        return this[row * colCount + column];
    };

    return points;
}

function createSun(position) {
    return new Path.Circle({
        center: position,
        radius: 7,
        strokeWidth: 2,
        fillColor: 'yellow',
        strokeColor: 'red'
    });
}

function getGravityVector(position, center, step) {
    var centerVector = center - position,
        dx = sign(centerVector.x),
        dy = sign(centerVector.y),
        points = [new Point(position.x + dx * step, position.y + dy * step)],
        minDist = Infinity,
        i, stepVector, dist, point;

    if (dx !== 0) {
        points.push(new Point(position.x + dx * step, position.y));
    }
    if (dy !== 0) {
        points.push(new Point(position.x, position.y + dy * step));
    }

    for (i = 0; i < points.length; i++) {
        stepVector = points[i] - position;
        dist = pointToLine(position, points[i], center).length;
        if (dist < minDist) {
            minDist = dist;
            point = points[i];
        }
    }
    return point - position;
}

gridPoints = createGrid(view.size, step);
createSun(viewCenter);

var focusRing = new Path.Circle({
    radius: 7,
    strokeColor: 'red',
    strokeWidth: 2
});

function createVectorItem(start, end, color) {
    var vector = (end - start).normalize(10);
    var vectorItem = new Group([
        new Path([start, end]),
        new Path([
            end + vector.rotate(160),
            end,
            end + vector.rotate(-160)
        ])
    ]);
    vectorItem.strokeColor = color || 'red';
    return vectorItem;
}

function sign(x) {
    x = +x; // convert to a number
    if (x === 0 || isNaN(x)) {
        return x;
    }
    return x > 0 ? 1 : -1;
}

function normalizeVector(x, y, length) {
    var nx, ny, tg;
    length = length || 1;
    if (x === 0) {
        nx = 0;
        ny = length * sign(y);
    } else {
        tg = y/x;
        nx = length / Math.sqrt(1 + tg * tg);
        ny = nx * tg;
    }
    return [nx, ny];
}

// Find distance from a line to a point.
// Takes two points (p1 & p2) that define the line
// and the point to find the distance to.
function pointToLine(p1, p2, p) {
    var n = (p2 - p1).normalize(); // unit vector in the direction of the line
    // Two things worth mentioning:
    // 1) (p1 - p) * n won't work as a dot product, the 'dot' method has to be used;
    // 2) (p1 - p).dot(n) * n won't work, because the first param has to be a vector
    return (p1 - p) - n * (p1 - p).dot(n);
}

var startPoint;
var endPoint;
var accelerationVector;
var velocityVector;
var startVectorItem;

function onMouseMove(event) {
    var point = getClosestPoint(event.point, step);
    focusRing.position = point;
    startPoint = point;
}

function onMouseDrag(event) {
    tool.onMouseMove = null;

    var position = getClosestPoint(event.point, step);

    if (startVectorItem) {
        startVectorItem.remove();
    }
    if (!position.equals(startPoint)) {
        endPoint = position;
        velocityVector = endPoint - startPoint;
        startVectorItem = createVectorItem(startPoint, endPoint); // draw velocity vector
    }
}

function calculateNextStep() {
    var vector = velocityVector + accelerationVector;
    startPoint = endPoint;
    endPoint = startPoint + vector;

    velocityVector = vector;
    createVectorItem(startPoint, endPoint); // draw velocity vector

    accelerationVector = getGravityVector(endPoint, viewCenter, step);
    createVectorItem(endPoint, endPoint + accelerationVector, 'blue'); // draw acceleration vector

    view.update();
}

function onMouseUp(event) {
    tool.onMouseDrag = null;
    tool.onMouseUp = null;

    accelerationVector = getGravityVector(endPoint, viewCenter, step);
    createVectorItem(endPoint, endPoint + accelerationVector, 'blue'); // draw acceleration vector

    setInterval(calculateNextStep, 100);
}
